
S1: 00:00:03.067 Okay. Hello, everyone, and welcome to our last brainstorming sessions about last week of the MOOC. So our first topic of discussion today will be about big codebases because as you may know, if you learn programming, then you learn about small programs mostly. You start with Hello Worlds, and then you are asked to do simple stuff about prime numbers or whatever, numerical easy stuff. And at this point when people are confident that you know loops and control flows, then people estimate that you know about the programming language, and so you know how to program. But in real life, you arrive in big codebases, and then no one prepared you for that. Because you see horrible stuff in big codebases. So, yeah. So the main topic will be how to find what you're looking for in big codebase, how to understand them, and how to make sense and find where you have stuff to fix in them. Does anyone want to give any thoughts on that? Okay, Olivier, since you are programming teacher in various languages, I guess.

S2: 00:01:27.470 I never went further than the while and the for loop. I showed some people recently - maybe you heard about that - a project that is called Gource. I don't know if I pronounce it well, G-O-U-R-C-E. That somehow can give hint about the evolutionary nature of Git repositories and projects where you see an animation of [commits?]. It's a 3D visualization, or 4D because time is involved, about commits happening in various branches of codes and people acting on that. So that kind of contextualizes what's happening in projects and so on.

S2: 00:02:28.357 Also, there are the tools of Bitergia like cauldron.io, I think, that somehow try to analyze the health of projects and so on. So maybe understanding that it's like an ecosystem or a city or stuff like that is a way to not be afraid because it's not because a city is big and lots of people [inaudible] in that city that you can't become a citizen and so on. So that's maybe for the general context, and it's not really a tool that you can use. But that's the way to illustrate that a big project is complex, and no one ever gets a comprehensive view or understanding of what's happening. So the second one is Cauldron. I am not sure.

S1: 00:03:29.643 So the main concept here to pass on is that you don't have to understand everything basically?

S2: 00:03:38.154 Don't be afraid. The basic stuff is don't be afraid about the complexity. Then if you have something to do, you're not afraid, but that doesn't tell you how to do it.

S3: 00:03:50.800 I think the concept of a city, it's a really good metaphor actually. It's true. You don't try to understand a full city before interacting with a few people there or even finding a job. So it's really nice.

S2: 00:04:02.840 Well, in IT careers we used to speak about urbanist as a position as a whole. I don't know if it's still used in companies nowadays, but people that look and try to think about the future by directing the teams to where you have to fix stuff and then change and replace. That's a metaphor. So I don't know about stuff, but I use a lot ACK. I don't know if you know this tool. So that's a completely different perspective, but it's like a grep on steroids or grep with superpowers.

S1: 00:05:07.811 Do you have links for Cauldron and ACK? Because it's not really googleable.

S2: 00:05:12.673 Okay, I'll try to do it. So Bitergia, cauldron.io. Oh, I should probably put that into the--
[silence]

S2: 00:05:58.857 So, yeah, I mentioned ACK. I don't know if you know about this tool, so that's basically--

S4: 00:06:03.103 No, what is it?

S2: 00:06:05.699 Grep. It was called ACK grep. So ACK may be the initials of the author. I don't know. You can do apt install ACK. And that's a great tool because it understands different syntax, different software language. So when you want to search for a particular word or variable name or stuff like that, it's better than grep because grep has a lot of false positives and so on, and also it can colorize. Maybe there are other tools. I don't know. But this one is quite versatile.

S1: 00:07:02.605 Honestly, the best alternative to grep that I use all the time is git grep.

S2: 00:07:16.360 So I will copy paste the description from the Debian package, so.

S4: 00:07:24.385 Beyondgrep.com?

S1: 00:07:30.758 Yep.

S4: 00:07:33.299 Okay. So what can you do exactly with this and with git grep? How do you use it to browse through a large codebase?

S1: 00:07:59.380 Me or Olivier?

S4: 00:08:01.296 Yes. You and Olivier because I've never used this kind of tool.

S1: 00:08:06.828 Basically, I use it to replace what an IDE could do, but it's way lighter. So I can find the uses of a function or stuff like that. Basically git grep is like grep but in a repository, so it only looks in tracked files. And it has options to deal with history, I think.

S4: 00:08:33.919 So is it semantically aware of the source code or not?

S1: 00:08:43.207 No.

S4: 00:08:43.510 Okay. So it's only text, syntax. I mean, not even syntax, only text [crosstalk].

S1: 00:08:51.330 Yeah. It's just pattern matching.

S4: 00:08:53.823 Pattern matching. Okay. All right. So, well, personally, I use an open-source tool also, Visual Studio Code a lot.

S2: 00:09:10.099 No, it's not open source.

S4: 00:09:12.039 It is 90% open source.

S2: 00:09:16.696 Okay. That's an interesting point for another week.

S1: 00:09:23.256 We don't have any more weeks. Loic will be happy that I cannot say, "We will see that next week," today.

S4: 00:09:36.332 So, yeah, I use VS [inaudible], but that's natural because I use VS Code.

S3: 00:09:41.420 But that could be something to include in one of the previous week. Though, it's true that having your tools being fully open source or free software might be an important concept to pass on, actually, including for the project itself.

S1: 00:09:59.203 Most project just tell developers to use the tool they are used to and don't try to force specific tool because some people are used to tools that may fit them.

S3: 00:10:17.044 It's not really in the sense of having to use the tools from the project or what the project mandates, but it's more that if they want to be able to-- when you become a contributor, it's because you want to participate to a project, maybe have better control over what you're doing, like interacting with a larger community than just what you can do yourself. And I think passing on the concept that when you use free software tools, it means that you can apply that to your whole stack to your whole environment of work. And if you have in the middle some proprietary tools, then this is something on which you will never be able to contribute, basically.

S1: 00:11:12.384 Okay. That makes sense.

S2: 00:11:13.371 That's an interesting discussion, but maybe back to the point if I may. I think an IDE is quite interesting, yes. I have used it, for instance, a lot on object-oriented source codebase because in some languages you tend to have tons and tons of source files and tons and tons of classes and stuff like that. And then just for basic flow of execution, you have like tens of different units involved like classes that call themselves and so on.

S2: 00:11:58.810 And so you tend to have-- for understanding just a single flow of execution till the point where you have a bug or till to the point where you would change something, you have to navigate through different subdirectories or different sub-modules where sometimes it's not really obvious if the back end or the front end [inaudible] but because the directories are a bit fuzzy or things are mixed together. So I think understanding what is modularity, what is the principle of isolating in an object stuff so that it is independent of the rest is interesting.

S2: 00:12:43.556 But compared to a big mess of code in a single file, which used to be the past. But then the problem is that you have tons of subdirectories where stuff happens. So if you don't have an IDE to navigate multiple tabs or multiple files at the same time, you have a problem of complexity because your brain basically only manages 5 to 7 units simultaneously. But in a real application, with an object-oriented approach or stuff like that, you have like 10 to 15 places to check to see to see if it happens there.

S2: 00:13:29.819 So I guess the IDE like, [VS Code?] or myself Eclipse, for instance, because I'm teaching with Eclipse, so I got used to it, is quite important. And Emacs for instance, I wasn't able to use Emacs for that because probably I didn't have the right tooling in Emacs to make sure I would be able to follow the dependency graph. But I don't know if it's a common habit or if people have a natural tendency to be allergic to IDEs, but for me an IDE is mandatory for such task.

S1: 00:14:19.413 I don't use an IDE, and I think most people in Inkscape don't use an IDE. And my window looks like the link I've put usually when I work on Inkscape. This is my real fourth desktop that looks like this.

S2: 00:14:39.153 But I would tend to imagine that you know very well the organization of your modules, submodules, and units of code and so on. So when you're doing something you probably have a--

S1: 00:14:53.752 I have an idea in mind. Yes, I know what I'm doing.

S2: 00:14:57.426 Yeah, so you have 5 to 10 locations where you think at the same moment. And those locations, they are in different buffers in your editor, and that's it. But for someone who doesn't understand--

S1: 00:15:05.877 It's more like I have a buffer of-- I have a buffer of the list of stuff I'm looking at. It's the third one on the screenshot.

S2: 00:15:23.319 And also something that is quite important for me sometimes is a debugger. Because I want to follow the execution. What I mentioned is that initially, I find a bug somewhere, but I don't know where the source code is. So I just have to put a breakpoint, and. But for that, I need to execute to follow the path. And so I need something dynamic. I need the execution. And I need the debugger most of the time to do that. So sometimes the source code is static, but your problem is dynamic. So to understand that you need to be able to transition from one model to the other. And if you don't understand the whole of all your needs, it's quite impossible. So my only way is to try and follow [several?] times, and then when I understand I can make a modification, [support?] something.

S4: 00:16:31.342 So we are talking about [rich?] tools. In a little bit what to make with these tools to find the source of the bug, which is the most difficult and challenging part here. We were talking last time about test cases, and even Marc said that Inkscape, they don't use a lot of tests. Less than 10 tests for the whole project. And also we were talking about debugging tools. But, Loic, you said that debugging is not so much used in bigger projects. So what is the difficulty here to find the source of the bugs, and?

S5: 00:17:26.919 It's a very difficult topic actually.

S4: 00:17:29.538 Yeah, you have to [inaudible]--

S5: 00:17:29.165 It varies a lot depending on the person, depending on the tools. Lots of tools were mentioned.

S4: 00:17:34.812 Absolutely. And I read stories about very simple bugs that took weeks to find the source, the root cause.

S5: 00:17:48.378 That I don't believe in.

S4: 00:17:49.434 And it was only value to put somewhere and you fix the bug.

S5: 00:17:52.256 There are two kinds of bugs. There are the bugs that you can reproduce all the time. And these ones, there is no way it will take you weeks to fix them. It will take you a day, two days, maybe. But no way it will take you weeks, no matter how big the codebase. I mean, you can figure out that the bug cannot be fixed because there is a much larger problem, in which case, yes. But you will find where the bug is fairly very quickly because you can reproduce it every time.

S5: 00:18:34.451 So a key, maybe, to start to understand the codebase, if you are after a bug, is to make absolutely sure you can reproduce the bug every time. I mean, unless I'm able to reproduce the bug, I don't even look for the cause because that's super difficult. And you can waste a vast amount of time if the bug happens every 1 in 10 times, and you don't know exactly when or why. You just have to wait until it shows again. So for the course and for our contribution, in general, we want a bug that can be reproduced all the time. And to navigate a codebase that you don't know, you don't want a bug that you cannot reproduce. And then--

S4: 00:19:28.363 So not only difficult to find a project but it's also difficult to find the good bug to fix.

S1: 00:19:37.194 I would never recommend a newcomer in a codebase a non-reproducible bug.

S5: 00:19:43.634 Yeah, exactly.

S1: 00:19:44.986 If some people interact and ask for a bug to fix, I will never direct them to something that cannot be reproduced reliably.

S5: 00:19:54.313 So then if you can-- in the past weeks you've learned to rebuild the project, you've learned to run the test if there are some, and then you are able to run the reproducer on the latest codebase. That's the first step of navigating the codebase that is making sure the bug is actually there because maybe it's fixed in the latest code. So if you are able to reproduce it on the code that you compiled or that you run on your own machine, you're in good shape because you're interested in one point. You don't know exactly where it is, but you know that you will find it because all the time, you can reproduce the bug. And then it depends on what you have in your hands. If you have a stack trace, then it's excellent. That's very good.

S5: 00:20:55.873 If it's a bug that you can only visualize because it's on a web interface, it's more difficult because if it's blue instead of green, then you're in trouble. And it's a very, very different problem. I would say there are two categories of exploration. One that starts with stack trace, and when you first enter a codebase, that's the kind of bug, that's the kind of behavior that you want to fix to learn the codebase. Otherwise, you have to understand things, and you have no way to know how they relate to the bug that you're experiencing. And that can take a very very long time.

S4: 00:21:50.647 So does it mean that the bug that has to be chosen has to be categorized as easy fix or a minor bug or I don't know, something like that?

S1: 00:22:04.653 Well, it doesn't have to, but we will strongly, strongly, strongly recommend that it's considered as such by the project.

S4: 00:22:14.384 Considered, but how do you prove this consideration?

S5: 00:22:18.220 By most of the time the bugs that are marked-- I mean, at least half the time, the bugs that are marked easy are not actually easy, so. And they are marked easy by--

S2: 00:22:30.097 The problem is that you can easily try to fix it, but it will break other things while you fix that. So it may look easy, but then the people say, "Okay, you fixed it but then you broke all the other," so.

S5: 00:22:48.335 So recently, three weeks ago, I entered a codebase that is 15 years old and has a lot of history, so it's complex. It's not huge, but it's really complex, and there is no documentation whatsoever. But fortunately there are a number of tests. So what I started to do to understand is I just picked. I run the test. I looked at the code coverage, and I spotted a place where some lines were not covered.

S5: 00:23:30.202 So when you add code coverage to a codebase, it's always welcomed by the project. There is no question, your merge request will go in. And so I went to add, to modify a test because I knew that in the process, I would learn a little bit about the codebase. And it turns out that [inaudible] learn anything useful this time, but in general, that's a good approach, I think. But I wouldn't say it's always the good approach because there are a number of conditions for that to actually work. So I think this week is a really difficult one.

S2: 00:24:20.144 I think it's interesting that you mentioned code coverage, mentioned debugger. You mentioned also the [backtrace?], so all that means that you have to be able to execute, and there is a notion of execution that may at some point fail. So yes, being able to execute but in a controlled environment, not the environment forced on [inaudible].

S5: 00:24:53.037 Yeah, right, and that's a mix of what people did in week four and five.

S2: 00:25:04.952 But when you mention what you did for discovering this new project, it reminds me what I did a lot in the past in trying to maintain a new software, stuff like that. There was no testbed, no code-coverage tools and stuff like that, so it was too hard to try. But what I did is just, I was trying to read the code, read the code. And then when I read the code, I spotted a lot of typos, mistakes, stuff like that. And then my process was sometimes to try and fix them as I was reading them.

S2: 00:25:47.335 So I don't know if it's a good thing to do because most of the time I would expect you to be forking the project while you're doing that because if you fix stuff that you read without knowing if they break in the real execution, it's probably dangerous. You shouldn't touch too much. But I have a difficulty to read something that is obviously wrong and not trying to fix it along the way, so maybe that's just me. I don't know. Maybe that's not an advice to give in a course.

S1: 00:26:19.422 I would actually give that as advice. Small documentations or small refactorings that has little chances to make large problems are actually, I think, a good way to find out where things are.

S2: 00:26:35.839 But do you expect a newcomer to get their contributions accepted if they're doing so?

S1: 00:26:43.106 That was the way I entered projects, by doing refactoring stuff. Not large ones. Well actually large ones, but after a small one.

S2: 00:26:58.231 I think you need to be confident, and you need to be very much welcome to do that, or the project needs to be very liberal because it's fundamentally risky as a newcomer to go and change and say, "Oh, you're doing bad stuff. I know how to do it." It's a bit, maybe considered rude by some developers if you go and break and refactor.

S1: 00:27:27.286 Yeah, at this point I knew people in discussions about the project. But basically the refactoring I did was in a C++ codebase, I replaced C-style linked lists with C++ vectors, and that made considerable performance improvements at basically almost no cost. Because it was used as a vector, but it was a C-style linked list, and that was very inefficient. So people were confident. I was confident. And so it went. I'm not sure it can be transposed to many other languages or projects.

S5: 00:28:15.118 So I tend to agree with Olivier that, in general, it's not welcome. In your specific case, you were bringing performance improvements that justified the refactor, so, yeah, it's a very good example. But what you mentioned also Olivier is being tempted to fix things because you wouldn't do it this way or because you see something that is obviously wrong, so you want to fix it. But you don't know if it will be welcome by the project just because you see it. And it's a way to discover the codebase, but the risk that your contribution will not be accepted just because there is no open bug is very high.

S2: 00:29:01.627 Yeah. And if you look at the commit that would result from such work, it's awful because you touch everything, and you're just fixing typos or stuff like that. But you are suddenly touching too many things. So if you just want to make small commits or coherent commits, it's impossible because you're just a [crosstalk]--

S5: 00:29:30.415 Oh, that gives me an idea. To discover a new codebase, there is a great way to do that. Say you go in a file, and you think it's about something, but you're not sure. Then you can look at the history of the modifications. And by doing so, you will not just be interested in how the file was modified but in which context it was modified. That is, if you look at a hash map, something very low level, and you say, "Why this latest change in the hash map?" Then you can take a look at all the commits that were involved in the merge request or the pull request and at the discussion around this merge request and pull request. And this discussion tells you a story about the hash map. And it will tell you, "Oh, this was in the context of improving performances. This was in the context of a security fix because there was a leak." And there you learn something about this file but in a much broader context. So it tells you something about the history of the codebase that you wouldn't learn otherwise just by reading the code. So that's very precious.

S1: 00:31:06.325 We mentioned that last time, so I actually had wrote it in the [part?] before you mentioned it. I put it in bold that a good exploration method is use your version control system and know how to use Git log on a file, Git blame to see basically who changed what. Git show to read the commit message and what they change at the same time. And Git bisect if you're good.

S5: 00:31:37.933 And the discussions, which unfortunately are not [crosstalk]--

S1: 00:31:41.020 Yeah, yeah, but the summaries of the discussions are in the commit message.

S5: 00:31:46.442 No, no. That's horrible because it should be but it's not.

S2: 00:31:53.318 Yeah, but in practice, Loic, I expect to be able to look at Git blame for instance when I'm browsing something in my IDE, but when I'm in my codebase, I open an IDE or Emacs or whatever, the description is probably in a different tool on the [Forge?], somehow. So particularly, it's not really easy to navigate both at the same time.

S5: 00:32:27.144 Maybe so, but it's most enlightening, especially when you try to figure out how a bug came to be. You navigate around the lines of code, and you say, "Well, it's here. It's happening here." Now, you wonder, okay, what happened in the past two years around these lines of code? You use Git blame, of course, and the commits as well. But you're missing out very important stuff if you don't look at the merge request and the pull requests.

S2: 00:33:02.081 Which brings us back to the power issues that you mentioned about big projects, where you see who sponsored the change that introduced a bug, and it's always the same employer.

S1: 00:33:21.155 How dare you use Git blame to blame people? No, yeah, I really think that Git log and Git blame are good tools to learn about the history of files and who changed them in which way. I also wanted to mention that some projects have infrastructure road maps, with things that they intend to do. And it could be a good way to find out what sort of change is welcome in a project. Well, it's a bit late now, but at some point, many projects in Python wanted to transition to Python 3 and welcomed changes that would ease this transition. And some projects do have a road map that say, "We are trying to do this long term. So this kind of refactoring projects are useful, and we would welcome them."

S3: 00:34:27.402 Yeah, and actually often those initiatives because it's a lot of small, simple tasks, but there's a lot of them, there tend to be a kind of a campaign associated with it, "Help us to port to Python 3." Like that can be also upgrades of Django versions. For example, you have to remove a lot of warnings and etc. And those are a lot of easy fix. And they tend to be more advertised, like going to the forum or to the mailing list, or sometimes the website itself. Could think to support those ones because that's kind of almost a subcategory of the bite-sized bugs at the beginning. And there might be also a lot of social support, like where people are ready to help with that, because they want to see the project complete.

S2: 00:35:21.017 And there is a timing factor that is quite important because sometimes you need to do something first so that people can build upon it, instead of mixing multiple changes at the same time. So yeah, those transitions API changes, stuff like that are quite important to monitor. And maybe not try to push your changes while some transition is in process. And you should have to have tested it before introducing new features, new stuff.

S5: 00:36:02.719 Right, I have this case exactly at the moment on a codebase where I wanted to do something, and then I looked at the commits in the past few weeks and discovered that someone was working in the same vicinity of the code. So I just postponed my work, and I was inspired to do so because this person touched the part of the code that I intended to work on. That was not on the road map, but that could have been shown on the road map too.

S5: 00:36:37.721 One excellent way to discover the codebase is, instead of taking notes about your understanding of the codebase, you try to fix a bug, even if you're not able to. That is, you take a bug that you know happens in this vicinity of the code, and you make an attempt to fix it with your current understanding of the codebase. And what will happen is either one of two things. Either nobody will answer your merge request, and so you're on your own. But by describing the problem in your own way by writing code that could fix it even if it does not work, then that helps you understand the code.

S5: 00:37:41.115 And second, if someone answers, it's likely to be someone who knows the codebase better than you do and will tell you, "No, no, no. If you want to fix that, it's not the way to do it at all. You're completely wrong," and that's very, very precious. And that also happened to me most recently in a Django codebase. I wanted to make a redirection with the middleware. That's perfectly fine to do it this way. And so I implemented it, sent a patch, and that person said, "No, it's way too complex. You don't have to do that. Do it here instead," and I discovered a new way to do that. So it's a way to induce people who know to help you understand the codebase by expressing your ideas or your understanding in form of not-so-good bugfixes. And a good thing is, you're not committed to fix the bug. You just attempted to. You failed. And you can close the merge request, but you gained something that is valuable.

S2: 00:39:00.299 Do you think you're a bit like asking people to do your homework? Because you do fix the bugs that you're not committed to and do it the wrong way maybe? I don't know.

S5: 00:39:14.981 If you do it in a very crap way, there is no way-- no one will answer you. So that's a complete waste. You don't understand anything, and no one tells you anything.

S3: 00:39:29.512 I would say that it even depends because it can be done in a deliberate way. I mean, it's often a good thing when you start working on something, even when you're really used to the codebase, to start by a WIP pull request, a work-in-progress request, where it is draft. It is not meant to be the final thing, but you want to pick other people's brain on how to do that. And as long as you don't send crap and say like, "Deal with that," I think it's something that people appreciate to be consulted early on some things so that they can actually influence the approach rather than having a big final pull request where they have to say, "No, that doesn't work."

S3: 00:40:18.584 And I think actually your approach on this, Loic, you could even combine the two. You mentioned that you realized you were working on something. You saw in the history there was someone working on that. You could actually ping that person on the [inaudible] pull request because you know that that person is actually working on that. I think, often, having a pull request is better because you have some code to show rather than just opening an issue where you're like, "I have that problem. How do I solve it?" you show that you have done some of your own work even if you haven't done everything.

S1: 00:40:56.943 A risk by pinging someone working around that code is, "Oh, I hadn't seen this bug. Oh, it's fixed now."

S3: 00:41:08.075 That is true. That happens even if you actually make the real pull request. Some projects have no idea how demoralizing it can be to a newcomer. You arrive. You open your pull request. You've done everything perfectly, and the guy says, "Oh, no I think there was a better way of doing this. By the way, this is already merged." When projects are like this I would actually argue to change project, honestly.

S2: 00:41:35.401 But how do you know-- how do you know a project behave-- I mean, behaves like this?

S3: 00:41:47.040 You can look at [crosstalk].

S1: 00:41:47.185 [crosstalk] and not the whole project.

S3: 00:41:50.362 It's true. And one way to check this would be to check the previous pull request like how do they deal with the incoming merge request. Like do they do that part regularly or not?

S5: 00:42:01.404 You see that really quickly by looking at the history of the pull request. You see the tone of the exchange.

S1: 00:42:09.510 You can also look at the list of contributors and see if there are only three contributors even if many people made merge requests then you know that they commit in their name and try to do things themselves.

S5: 00:42:26.798 There also some cases where the codebase is just too big. You have to find your limits. I mean, everybody has limits in what they can understand and keep in their mind. And so you have to learn how much you can take in and in how much time. It's like running. You have to know if you will go the distance or not. That's also why this is not something you can learn in a few weeks. It takes years for you, as a programmer, to know your ability to absorb a new codebase.

S2: 00:43:21.058 And you get older and your limits are closer to you. [laughter]

S3: 00:43:26.581 Well, [inaudible] was something I've seen a lot is scope creep. In a pull request, someone will start with some very small change. I don't know, they will want to fix a bug in the static asset pipeline or something. And then they realize, but this pipeline is actually crappy. I'm sure we could do better. And they start to replace a part of the pipeline. And then they are like, "But why the build process to make way for this," and they start to end up with like a monster pull request, which they will never be able to merge. So maybe, related to what you just said, Loic, knowing your limit and knowing where to stop in your process, like start with something really small and stick to that. Even if you see a lot of dirty stuff everywhere.

S5: 00:44:16.667 Then it's good because you understand more, but it does not belong in a pull request. Again, recently I did that kind of thing, so I made different pull requests. It was actually not pull request. It was patch, attached to a [Redmine?], very old-fashioned. But there was a documentation patch, a build patch, and a coverage patch, and a bug-fix patch. All of them reflect part of my understanding of the codebase as I went along. I was not focused on the bug fix because I had to understand more, and that translated into patches. But it has to be in different patches, of course.

S3: 00:45:08.222 Oh, and, also, because in each case I'm guessing you stuck to fixing a small patch. Like the type of scope expansion I'm talking is they replace the whole static build mechanisms. Especially when people are not used to contributing to projects, they imagine that they can do like in their last job where they arrive, they say the build system was not good, or they imagine it was not good because they didn't have the history. They replaced it. I see people doing that [inappropriately?] all the time. They come. They want a clean slate. They replace stuff because they think they have done better. If you do that with a big project especially, a big open-source project, that won't work because you'll need a lot of politics to be able to replace tools or base stuff, and so that will always fail.

S5: 00:46:04.105 That's true.

S2: 00:46:05.556 Or you create a fork along the way.

S5: 00:46:06.915 Or you create a fork. But that's a very, very good way to understand the codebase. You throw away the old codebase. You make your own. And that you understand.

S3: 00:46:22.141 Yeah. But what I've seen is that people really think it will work out. So when it doesn't they get demoralized, and they just quit the project. So, yeah. If you do that with an educative goal knowing that you won't really be able to replace it in the project, or you won't be able to do immediately something of that scope as your first contributions, why not? But I think people do that because they think they know better than the project, and that's why it tends to fail.

S5: 00:46:52.331 Yes. But let's say this is never the case. Let's give them the benefit of the doubt. And let's say it comes from a will to understand. So that is of course a little bit contorted because they do not actually understand [they replace?]. But it's a fair point. One advice I would give to someone, and that's because I'm very old, as Olivier kindly points out every time we meet, one way to accept the code from others and to be more at ease in a codebase that is not yours, is to pretend that you forgot about it, that you wrote it 50 years ago. You completely forgot everything, but it's you. And it's not really fake because it will happen to you unless you quit developing. There will be a time where you go back to an old codebase of yours, and you will have forgotten everything. And then you will want to understand it. It's like an old story told to you, or maybe you want to tell the story to your grandchildren. So it's a way to accept a codebase and resisting the urge to change it to understand it, is to pretend that you wrote it in the past that you don't remember. I'm sure--

S3: 00:48:24.428 That only works with people who are as old as you, though.

S5: 00:48:27.979 Well, there are none of course. [laughter] But 50 years from now it will work. Now that's--

S2: 00:48:35.803 I guess this is quite interesting because you can be very conservative in I put so many efforts in trying to make it run and don't break it. And then someone has to take over at some point because it's too rotten, but not to be discarded completely. But there's a major refactor to do. And so you don't have the guts anymore. You don't have the passion anymore. You don't have the time anymore. And you need young blood to do that. But when would be the right moment? Certainly not for [inaudible] bug. Or changing the language, or I don't know, making something distributed, like breaking everything and having to stop all the developers in their habits to change to adopt a new way. It's something you'll never do. Or you get amnesia, as Loic mentioned, and then you're able to be interested again, but.

S3: 00:49:37.399 That's a good point. It's true that you do need that sometimes. And there was actually a case like that very recently in Open edX where the way of deploying things was kind of like an old cranky way that nobody was really happy with. And someone came in and created like a sub-project for this. Tutor, for Loic who knows the project a bit more. And that is becoming actually the main way of doing things. And if nobody added that fresh blood way of looking at this, it would probably be stuck in the old ways.

S3: 00:50:15.208 At the same time, it wasn't that person's first contribution. That was a developer who had worked with the platform for years, actually as part of FUN, and had been really frustrated and going out of that. [inaudible] said, "Okay, I know what's the best way to do these." And he worked on that. So I think it's important to have that, but there is a sweet spot in terms of when you can start to be that ambitious. You need to know the project well first.

S2: 00:50:42.765 And in any case you are able to fork somehow. And maybe finally it's your fork that succeeded, and people will join you in the end because they thought you were crazy, you were breaking the project, you were forking. And in the end they just look at your contribution, and they say, "Okay. Let's put our egos apart and join back this new version that is more comfortable [inaudible]."

S5: 00:51:07.531 So in the context of this course, the key point to convey is to kind of help developers accept the codebase. To understand that they-- to be--

S1: 00:51:27.192 Be humble.

S5: 00:51:28.979 That understanding the codebase is something that can be pleasant. Something that is extremely frustrating when reading code is when you wonder, "Does it work or not?" Because there is nothing less useful than understanding code that does not actually work. And for that, not having test is super stressful. Because if you spend two days understanding code, and you realize that it never worked because no test shows you that it does, it's super stressful. Where the code can be super crappy, if a test shows you that there is coverage, so it actually works, then it worth learning. You can accept it. Okay, it's ugly but it's someone's baby. I will not kill it.

S4: 00:52:25.280 I was thinking about two things. The first is sometimes I try to optimize the time between, how do you say, you make the bug happening and the time where you change the code, and you can make the bug happening again or not. And sometimes it takes a lot of time because you have to build the thing. You have to deploy something. You have to na, na, na. I don't know. And sometimes if it takes five minutes from when you change to code to when you want to try the new code, and it's too long. And sometimes you are desperate because you only try the smaller thing, and you have to try again, and you have to-- five minutes more, and etc. So the time between-- so I don't know if there is a definition of that time between when you change the code and when you can execute it to see if it is working. And sometimes it's difficult to shrink that time. And that second thing I was--

S5: 00:53:40.667 It's true that it's something to learn to work with. And it's a very good point because it has to be somehow in this week probably to [get?] that you must be patient, and maybe suggest activities between the time you modify the code and the time that it takes for the test to show you that it runs because of compilation or whatever. Then how do you usefully use this 5 minutes, 10 minutes, half a day, so it's not frustrating to just stare at the screen and do nothing?

S1: 00:54:24.954 You can already start to look for the potential segfaults that you will having added. And I thought--

S2: 00:54:36.692 Remi, you had another idea?

S4: 00:54:40.021 Yes, and I am thinking very hard because I cannot remember.

S2: 00:54:44.423 It's what you drank before the meeting, maybe. Sorry.

S4: 00:54:49.581 Don't say anything.

S1: 00:54:51.761 And the second thing you said about it's 7:30, so it's time to move on to the conclusion.

S2: 00:54:58.130 Just to make sure, did anyone mention that writing documentation can be a good way to understand the code, like documenting the architecture, documenting the big pauses, documenting the [crosstalk]--

S4: 00:55:09.757 Now I remember because of your [inaudible]. So it's really [crosstalk]--

S2: 00:55:12.269 --because I think it's [inaudible] essential sometimes to give that contribution. You don't fix stuff, but you document how stuff is built, working, and so on. And that's a contribution that allows you to make bug fix later.

S1: 00:55:27.400 I actually wrote that in the pad as an exploration method. Document things.

S5: 00:55:34.733 Thanks for that.

S4: 00:55:35.792 And I was thinking if there is any methodology, a generic method or process, or I don't know, like, abstract. An abstract way to start finding a bug. I don't know if there is some resources already existing to-- the best practices on how to--

S2: 00:56:04.934 You mean, identify a bug among all the bugs that are listed in the bug tracker? Or how to reproduce a failure and isolate the guilty source code?

S1: 00:56:17.791 How to find the source of a bug, or how to find the--?

S4: 00:56:19.852 Yes, the source.

S1: 00:56:22.041 Well, for me, it's breakpoints.

S4: 00:56:30.330 Breakpoints?

S5: 00:56:31.949 It highly depends on the bug.

S1: 00:56:33.809 Yes, it highly depends on the bug. A performance bug for instance, you have to perf or profile the executable, and then you find what takes time. A crash, you just GDB the program and back trace it, and then you're half-done. And all the types of bugs in.

S4: 00:56:58.400 So we should have some kind of categories of bugs.

S1: 00:57:04.292 The taxonomy of bugs?

S4: 00:57:05.710 Taxonomy of bugs and taxonomies of--

S2: 00:57:10.701 There are papers on that. For instance, it can be measured as the time to fix. From the time the bug is identified and reported to the time of release and stuff like that. Or you can [collaborate?] with the amount of effort, but you can also look at the discussion. But there are super bugs. Yeah, there have been a PhD thesis on such a matter for sure. In open-source projects, of course, because here you have data to analyze, but also probably some internal studies at Boeing in the '60s and so on.

S4: 00:58:00.918 So regarding this and maybe to conclude also that that will be a transition. We might think about failover activity. Rather than fixing the bug, doing something relevant to make the projects still-- how can I say that, to--

S1: 00:58:29.256 To further project?

S4: 00:58:31.211 Yes, kind of.

S1: 00:58:32.509 But even if it's documentation, it ends up as an accepted merge request, right?

S4: 00:58:41.092 Okay. Yes, but we say find a bug, and find the source of the bug, etc. And we don't specify which kind of bug is it in the documentation, in the source code. We say it's in the source code. So we have to find some metrics that says, "All right, if during that period of time, it didn't happen, that you didn't find the source of the bug, then maybe you should do something else." And when we should have this if something else for the whole MOOC.

S2: 00:59:35.229 I guess trying to confirm that the bug is indeed fixed is some valuable contributions sometimes. It's a bit like you provided the pull request, and someone just say, "Oh, no but it's fixed in between. I don't want your contribution." But sometimes it's, "Yeah, I did retest on another environment, and I can confirm that, yes, indeed it is fixed." So that's a valid contribution, so. But it takes a lot of effort, and there is no trace because maybe you just had to prepare some data to rebuild and so on. And there is no trace, no commit, but still you have a valid contribution in the bug tracker, in the issue tracker to say, "Oh, I can confirm that it works also on Windows or it works also on the next Debian or whatever." So that can be as valuable as providing the one-liner that fixes the bug, sometimes.

S4: 01:00:33.081 Marc is doing that. Okay. Maybe.

S1: 01:00:36.749 I think it will be hard to motivate people to fix bugs if we have to say, "If you cannot, then just document what you've tried to do." And that's it.

S4: 01:00:53.351 No. It has to be explicit, I think. The homework will be explicit on the fact that it is difficult to find a project, to find a bug, to debug and find the source, etc., so they will get a sense of the difficulty anyway. Some of them will have very, very difficult bug, and they won't be able to fix and to find the source. Others will be lucky. It's a little bit like lottery. And you will be lucky.

S5: 01:01:25.623 I don't get it. It's not the lottery. How do you make it a lottery? How does someone come to have a bug that they cannot fix? How does that happen? Can you explain that?

S1: 01:01:38.156 It happens by the lack of communication, I think, and.

S5: 01:01:41.583 In the context of the course, I mean.

S4: 01:01:49.955 If we do everything correctly from the beginning, each step, then we might reduce the probability that student arrives at the end of the course with a very difficult bug on a complex project and completely lost. Indeed. But we have to make sure that the whole MOOC filters this kind of situation at each step.

S1: 01:02:26.321 In my opinion, if you should get to the stage where you actually interact with the project community, then you're on the good tracks to be told that you are going into a hard bug.

S5: 01:02:40.882 That's true. Yeah. I get what you're saying now. So filter is actually an excellent word for that. Maybe every week, questions or verifications towards spotting and filtering out bugs that are not fit. And so, every week, you will get warnings that the bug you chose has the wrong qualities for various reasons. And every week, it will tell you a new reason why it's not a good bug. Yeah. That's a good point. But the filters have to be designed.

S2: 01:03:28.427 Don't we have the risks that there's just too many people working on the same projects and the same bugs at the same time, and that results in duplicate work and stuff like that?

S5: 01:03:38.431 No, because the first thing is you grab the bug. You say, "I will work on that." And if you don't have the rights, you [comment on it?], so that's an easy one to fix. Now the more interesting one which Remi was pointing out is, how do you convey the thing that it's probably too difficult? There could be a list of questions that you can self-evaluate, and say, "Is this bug on the road map? Has this bug seen activity in the past month?" If not, that's not a good sign. Have you seen activity from the bug reporter recently? That is, can they provide more detail?

S2: 01:04:33.432 Have you seen bots closing the bug because of lack of activity?

S5: 01:04:40.554 For instance. And so you can have sets of questions, and some of them depend on the social context. For instance in the social week you could say, "Was this bug filled by someone working on the project? Do they have commit rights on the project?" Because then you say, "Okay, this bug interests someone who has commit rights. So that's a good point for the bug."

S1: 01:05:09.637 And usually, it's project members that files easy bugs.

S5: 01:05:15.143 Usually too. Yeah.

S4: 01:05:21.506 Is it a real bug or a small enhancement of the software also?

S5: 01:05:27.189 Yeah. Then you don't want enhancement. Yeah, and if every week you ask at least five questions about the bug in this way, and they are not really difficult to figure out, I don't think you can end up with a bug that fails all the questions at the end.

S4: 01:05:54.552 Maybe we can add questions dynamically if we discuss with the community and have new ideas. So, yeah, indeed I think we should have this kind of questioning at the end of each week module. So we have somehow to ask a lot of questions for the students from the beginning to the end. So from the community to the project to the source, the building, the bug, everything. So we have to have a list of questions for each week.

S5: 01:06:39.696 Yeah. And so if they end up, at the last week, with a bug they cannot fix, and they answered wrong to all the questions, didn't change their bug, I mean there is nothing you can do to help them. They will fail, but they will know why. Because it ticks, it's no, no, no, no. Okay. Well, so, it does not work. Nothing [crosstalk].

S4: 01:07:00.862 Yeah. And it's kind of proof that they worked well from the beginning because they have answers to all the questions. All right. So yeah. And we should conclude. That was the beginning of the conclusion on what to make for the whole MOOC to make the [foreign] I don't know how to translate that in English.

S2: 01:07:34.114 The breadcrumb?

S4: 01:07:36.952 Mm-hmm. So as a reminder, the first brainstorm was about the structure. So very quickly, do you think it is still relevant? You were there from almost the beginning. And do you still think it is the right order, or should we modify the structure? Or globally do we miss something very big, or is it okay?

S2: 01:08:10.951 We should do it and expect people to file bugs if they don't like it, and then we'll fix them. [laughter]

S4: 01:08:18.777 Yeah?
[silence]

S4: 01:08:32.858 And so, Xavier, you were expecting-- so you were working on almost a same kind of project, same kind of course, on how to contribute to FLOSS projects, more specifically for edX I would think, but. So do you see something big missing here? Or would you change something in the whole course, in the whole structure, or?

S3: 01:09:08.679 Missing, No, I don't think so because we've covered a lot of ground. I guess in terms of structure, my only remark is kind of the same from the beginning - I'm kind of a broken record - I think that the earlier we get people to do things, to talk to people, to do a small contribution, something like that, I think the more likely they are to actually get something done and merged by the end of the course. So that's the big thing I would pay attention to. But I think generally we have a pretty solid structure. I'm super happy actually with what we have discussed so far. I think there is a lot of really good and in-depth materials. And I just wish I had got a course like that, even with just that material, even just those meetings that we've had so far. If I could have watched that when I started contributing, that would have changed quite a lot of things. So yeah, that's pretty cool.

S4: 01:10:10.329 All right.

S2: 01:10:12.838 We are only missing a few beers but so far-- [laughter]

S1: 01:10:16.708 Agreed.

S4: 01:10:21.380 We will take some beer together for those of you in France, so. In Paris, almost.

S1: 01:10:31.729 Yeah. We can organize a workshop about the MOOC before it's launched.

S4: 01:10:37.551 Absolutely.

S3: 01:10:39.838 Definitely. And I mean, then there will the actual run of the course which will be interesting also to maybe have some real-life meeting if that whole COVID thing is done by then. It's not a really nice thing. Some more beers coming up I would imagine.

S1: 01:10:57.529 I think there is a big question, how do we continue? Do we organize other video-conference meetings? At which frequency? And on which topics? And how do we structure each week? How do we share the work? Who does what basically, from now on?

S2: 01:11:22.530 My guess is it's too late today, and we talked too much to be able to continue with this big question.

S1: 01:11:32.116 Yeah. But so we can plan one more video conference to discuss this very topic. Planning one meeting is just small enough task to fit in.

S4: 01:11:42.192 We have a road map already. We have [inaudible] objective of opening the course. When do you want to open the course, Marc? That will be the starting point of the following road map.

S2: 01:11:54.635 Do you mean you are a goal-based release or you're a time-based release?

S4: 01:12:01.220 Time.

S1: 01:12:03.421 Ideally this year.

S2: 01:12:06.258 Okay. So we release when it's time.

S3: 01:12:10.037 Yeah, I would vote for that, otherwise it never ends. And also even if we have a full meeting to discuss that afterwards, maybe a couple of comments on the next steps. One, I think it's great to have had a lot of synchronous time because, I think, probably we would not have surfaced as many things if we had done it differently. That said, I think it's also going to be important to have an asynchronous component to be able to do that, especially because the person that I'm thinking of within an open [inaudible] to help on top of what I do, is in Australia. So finding a common time might be really difficult in that area.

S3: 01:13:06.245 And also I think there are quite a few things on which we can actually move forward like I think we have a lot of issues on the bug tracker that we can definitely discuss then. And often, the way I like to approach a synchronous versus asynchronous, is to do things asynchronous first. So try that. And if it gets stuck, or if we realize we need synchronous time to explore subject or something, then it can be something triggered for that specific time rather than the other way around. Because when we do things synchronously first, the result tends to be that it's very difficult for someone who is not there at all the regular meetings to actually do anything. So that's my point of view. We can discuss that I guess next time, but I just wanted to mention that because that's going to be a concern on my side.

S4: 01:14:00.535 At least we should have--

S6: 01:14:01.515 I have [inaudible] one of the questions is maybe trying to have more in from the students for example your students at [inaudible] to see how they would interact with the content, and what is their opinion on the whole thing in general. Because, yeah, unfortunately, we didn't have a lot of actual students who could participate in this discussion. So maybe it will be interesting to see their answer before proceeding to next steps. Maybe about an overall structure of the course for example. Or maybe prototyping a short sequence of the course and testing it out on someone to actually see how it would work because we had a lot of very interesting discussions, but most of the people who participated are not necessarily the target public. So there is a chance that we might be [missing?] some important information.

S3: 01:15:09.998 I would plus one that.

S4: 01:15:11.605 [crosstalk].

S5: 01:15:14.452 Excellent point.

S1: 01:15:19.212 Yeah. Olivier, what do you think about the contents of the course?

S2: 01:15:26.085 It's quite hard to have an idea because I don't really know the subject yet. However, I think it is well organized, and it is well structured. So if a structure is announced at the beginning of the MOOC, it is easy to see where we are going with this. And I like especially the activity part, the idea of having one [inaudible] to do, and in each module, we do somethings to get to this goal. So this is really interesting I think.

S4: 01:16:22.392 Okay. Thank you.

S1: 01:16:24.585 It's also true that this week, we did not talk so much about interactions or activities.

S4: 01:16:36.213 Sure, it's not so explicit. But since they get in touch with the project community from the beginning, of course they have to maintain the interaction through the modules and especially at the end when they want to find the source of the bug. And maybe we could-- yeah, we could focus more, like do some focus discussion on what kind of interaction we could do more specifically for each week etc. But, yeah, that could be more asynchronous maybe work. More deep work, written work.

S5: 01:17:25.219 One activity which is good in this context is to explain how code works. Kind of a code walk. You explain how it works with links to the code. And you publish it somewhere.

S2: 01:17:41.072 It reminds me. Did we speak about this very interesting technique that is the duck debugging, no? You have a rubber duck, and you talk to the rubber duck, explaining the bug. Where the bug comes from. What the code is doing. And while doing so, you understand. Because when you explain to someone, it makes your brain understand more efficiently. So it's a very well-known technique or not so well-known technique. The rubber duck debugging. Let me find the link.

S4: 01:18:12.542 Interesting.

S5: 01:18:15.103 But then, you have to say that you turn into a rubber duck at the end.

S2: 01:18:21.245 Well, but there are nice things that you can do with the rubber duck if you have a-- anyway, rubber duck is a [inaudible].

S5: 01:18:28.874 [inaudible].

S6: 01:18:29.248 A perfect example of an activity that can be easily tested before being deployed in the MOOC because it's just, yeah, somebody can play the role of a rubber duck, and you can see how effective it is.

S2: 01:18:42.432 No, no, no, no. If it's a slave, it's not a rubber duck. Or if it's your girlfriend, it's not rubber duck. No. No. That's not the same. So the website is rubberduckdebugging.com.

S3: 01:18:56.426 You could still have it as a field in the course. Because then nobody has to listen or to grade it, but you still have to describe it there. That's still the same principle.

S4: 01:19:10.216 So yeah. If you have any idea of name of activities or methodologies, they are welcome as you did, Olivier. So any other idea resources. The GitLab is open, so.

S1: 01:19:28.164 The idea at the beginning, was to basically take two or three projects in several languages and ask them to find exact lines that prints a message in the console or does somethings very precise. And maybe for one of the project, find the lines that actually crashes when you do a precise set of steps so that you can introduce people to find sources of bugs basically, or. So basically, you can walk people from given commit of a given project to explain how to do a bug trace for instance and how it helps you to find where a bug comes from. So you trigger a bug.

S2: 01:20:26.958 Sometimes people are doing Twitch, live debugging or live hacking, where you can see them do. So of course sometimes people with a lot of experience, and probably that's for expert audience. But that could be interesting to see, let's say, Xavier explaining on Twitch, how he fixes something, or. And then Marc and then Loic, and then we can compare the approaches, and maybe the students could spot common patterns or common techniques and stuff like that. And it could be also a live event, maybe, that can attract a bit of a sense of community for the participants.

S4: 01:21:09.956 I know that some companies do that, that once every week, they ask their employees to stream live what they do on their screen for one hour. Publicly, I mean, and it's very interesting because sometimes I connect and I see what they do. Sometimes they browse the internet, searching something, do some coding and stuff. I don't know how we could implement it, but that's a very interesting idea.

S2: 01:21:48.557 And also, it could demonstrate the use of the tools like, "Oh, what is [linger?], with this window, here? Ah yes, it's the magic grep tool,'' and so on.

S4: 01:21:59.947 We could even say that, whenever they do some activities, we ask them to stream what they do live on a streaming platform.

S2: 01:22:11.147 That's something that music learning applications and sites tend to use to motivate. You know because you're playing piano, Remi, I guess. So I'm learning the guitar, and I'm not yet able to stream, but soon, maybe. So maybe we can think of something like that. Nice.

S4: 01:22:33.792 Very interesting.

S1: 01:22:38.752 It could be.

S4: 01:22:40.910 Also, I was thinking we should maintain a regular kind of newsletter. So we have this Matrix forum for the MOOC. We have the GitLab repository. We will have somehow, soon. What about a mailing list or something more?

S1: 01:23:06.275 We can have a blog generated by the GitLab.

S4: 01:23:09.406 A blog. Ah, that's interesting.

S1: 01:23:11.140 So we can have a moocfloss.gitlab.io/blog or slash whatever.

S4: 01:23:16.290 Yes, let's think--

S2: 01:23:17.315 Isn't this the latest trend in social marketing stuff all the hipsters of the Silicon Valley are just rediscovering the mailing list?

S1: 01:23:28.264 What, blogs?

S2: 01:23:31.780 Or blogs?

S1: 01:23:32.905 Blogs are old.

S2: 01:23:34.113 No, the mailing list is the trendy thing at the moment, I guess. Blogs are [old?].

S6: 01:23:39.379 The latest thing is the Clubhouse, but that's very time-consuming, isn't it? Do you know Clubhouse?

S4: 01:23:46.163 Clubhouse?

S6: 01:23:46.781 It's like voice chat. That's the last thing from Silicon Valley. It's kind of like a, yeah, group, probably talks from some mainly Silicon Valley celebrities that a bunch of people can listen to live and sometimes chime in, and there are guests who are invited, and stuff like that.

S1: 01:24:07.359 Oh, it's a Twitch stream with guests?

S6: 01:24:11.207 Yeah, but with--

S2: 01:24:12.113 Only audio?

S6: 01:24:14.011 Yeah, only audio.

S1: 01:24:16.716 So a podcast?

S6: 01:24:19.415 A live podcast, yeah, I guess.

S4: 01:24:22.703 Clubhouse, something.

S2: 01:24:25.866 Clubhouse. But you need an invitation, so you need to know celebrities to get in and so on. [laughter]

S6: 01:24:31.368 Or just regular people who are there. I have two invitations if someone wants. [laughter]

S2: 01:24:36.668 Ah! Ah! Ah! [laughter] Anna! Anna! I'm your friend. I've always been.

S6: 01:24:43.183 Yeah, but I think another problem is that it's only available so far for Apple products.

S2: 01:24:49.127 Ah crappy [inaudible] software.

S6: 01:24:50.551 Yeah, I don't really think that it [crosstalk] and also not probably the public of your MOOC, so, yeah.

S3: 01:24:58.844 Yeah, on the discussion side, I mean, we already actually have, with the issues, I think, a good [area?] which is kind of almost forum-like, in a way. One thing also to consider is that later on, we'll have a course itself, where there will be the students and etc. And one of the main ways of interacting between students in the course is a forum. So maybe having a forum, which is the same between us and one in the course could be a good way to bridge that gap. And one thing to know is that in Open edX currently the forum is not the greatest feature of Open edX.

S4: 01:25:39.119 Yeah, I hate it.

S3: 01:25:40.808 Yes, but there is good news, which is that there is work being done to integrate with other forums, including Disqus. So if we started with Disqus, potentially it could be integrated in the next course in the near future in the coming months.

S4: 01:25:59.042 Right, and do they have the same as GitHub discussions?

S3: 01:26:05.377 What do you mean the same?

S2: 01:26:07.243 [inaudible], no? Ah, you mean in [GitHub?]?

S3: 01:26:18.979 So I don't know exactly [what that being?] in GitLab must be issues, but I'm not aware of a forum feature, maybe like a third-party service, you mean?

S4: 01:26:31.797 No, integrated. I mean on GitHub, they have GitHub Discussions. So it's a new tab that looks like issues, and it's almost like issues, but it's not talking about issues but talking about general discussion on the projects. And I don't know if we can activate the same kind of mechanism on GitHub-- GitLab, I mean.

S1: 01:26:50.777 It's just bugs that are [tagged?] discussions, right, internally.

S4: 01:26:54.130 All right, we could do that.

S1: 01:26:56.978 Just like merge requests are bugs that are tagged with merge requests and linked with a pull request.

S2: 01:27:04.514 But on your initial question, Remi, was it, do we need to have kind of updates on the project, general mailing list, more than a discussion forum?

S4: 01:27:13.874 Yes, yes, absolutely.

S2: 01:27:16.373 So for people that are interested but not participating really.

S4: 01:27:20.767 Yeah.

S3: 01:27:21.784 Yeah.

S2: 01:27:24.042 If someone wants to dedicate time and effort to do so, I guess it's interesting. But maybe we need to have more content, more detailed stuff before [inaudible].

S3: 01:27:41.255 Well, again, if we do-- I mean, for now, I think we must focus on specific items. So at least as far as I'm concerned, I think, probably starting by using the GitLab issues could be a first step because we are not actually really fully using that. We have a connection, but we are not really having discussions yet there. And if we need to or if we start to grow and want to have more general discussions, I think probably more than a mailing list, a forum will be more friendly to users. But I guess it's a tool discussion, so that's probably a broader discussion.

S4: 01:28:21.845 Okay. So let's start with GitLab issues, and then if we grow up a lot then we will think about forums.
Sure. All right. Should we conclude now?

S6: 01:28:41.396 So was the plan for-- do we have an idea for a next meeting, or are we doing this asynchronously?

S1: 01:28:51.370 We could set up a progress meeting in one month or something like that.

S4: 01:28:57.696 Yep.

S1: 01:28:59.324 But it leaves a lot of time for discussions and progress and if we need-- in time there will be things between people involved in specific weeks or specific tooling then we can set up that independently from the issues.

S3: 01:29:17.175 Yep. But are we doing the meeting about setting up the next steps because that's the thing that we are not clear about yet, right? Or do we do that asynchronously?

S1: 01:29:33.964 Remi, what do you think is best for next steps?

S4: 01:29:41.116 I think we have to work a little together, Marc, to get all the transcripts and make them more structured. To gather all the data that we have and make it more information and knowledge kind of. So the next step, for sure, it's that. And produce for each module and for the whole course a new version on GitLab with everything that we've talk about. And I think then, the next step after this is to have a meeting on discussing about this new version, stricter version of all the data that we got.

S6: 01:30:35.035 Yeah. And then maybe go more to the depth of their activities that-- like every part that you have and every media that you use, so you have a clearer breakdown that you can use to [inaudible] production base.

S4: 01:30:49.957 But, Xavier, if you want indeed to have a synchronous meeting for doing that, we could do that together if you want. [inaudible].

S3: 01:31:04.867 Yeah, I was not really necessarily asking for it. I was just reminding what was left in the air in terms of next step. I think on my side, the most important part is to be able to follow what's happening. So I don't know how you are going to have those discussions. But if you do them asynchronously in writing, then I'd love to follow that and [inaudible] if necessary. If you do it more synchronously, then definitely it's better, I think, to open it to others. So that can be in another meeting. I don't want to limit anything. I think the important is that it's as inclusive as possible. That would be the thing I'd care about.

S4: 01:31:50.808 Very good points. So if we did work synchronously internally we will have to find a way to write down precisely what we said, what we decide, of course. Or we could just record what we say and put a video or a audio of what we said. I don't know. We'll have to think about, yeah, being inclusive from starting today. Like we cannot do some work that is not written or not open to everyone. Yes. I agree.

S3: 01:32:36.661 Cool. Yeah, that's an important one. So, yeah. Thanks for that. Especially because I know that it's an effort to do that as well because often the fastest way doesn't include that. So, yeah. Thank you.

S4: 01:32:50.011 Yep. Absolutely. Yes.

S2: 01:32:52.200 And in English, then?

S4: 01:32:55.140 Yeah. We'll try to do it in English [inaudible]. Yes.

S5: 01:32:59.448 Could you [crosstalk]?

S3: 01:33:03.502 Yeah. And by the way, so Jill who is going to try to [default?] on our side is start looking at what we've done already so far, the transcripts, the meetings, and etc. starting next sprint. So next sprint is two weeks. It's from the 8th to the 22. So like basically in the middle part of this month, she will be looking at that. She might start to pop up here and there asking questions or maybe potentially even doing some contribution if there is [matter?] for that. So just it's to give you a head's up about that. And she's Australian, so she will not speak any French. So that will force us to be more strict about that, I guess.

S4: 01:33:51.811 Perfect.

S2: 01:33:51.707 As long as she speaks BBC English, there is no problem.

S3: 01:33:57.068 Yes. She is originally American, so she doesn't have the Australian accent. Don't worry.

S1: 01:34:03.087 But we don't speak BBC English. [laughter]

S3: 01:34:06.971 Not at all.

S2: 01:34:09.242 Don't we? Come on. Okay.

S1: 01:34:15.146 Okay. So I think it's time to stop for today. So we will keep you informed about everything we do of course. Make sure to follow the project on GitLab so that you get notified if we open issues.

S2: 01:34:31.806 Make sure to subscribe to our YouTube. No. No. No.

S1: 01:34:34.419 Yeah. Subscribe to our GitLab and enable alerts, so.

S4: 01:34:41.400 And also the chatroom on Matrix.

S1: 01:34:45.595 Yep. Maybe we could have a bot that posts on Matrix what happens on GitLab?

S4: 01:34:50.255 Ah, good point. Yes.

S1: 01:34:54.851 I will look into that at some point so that new issues get a notification. I'm pretty sure that exists.

S4: 01:35:02.678 Thank you, everyone. It was really a pleasure to meet you all from the beginning. And it was very interesting with so many ideas. We have work for a few weeks now with Marc and others, of course if you want to join, to structure everything. And we will make sure that everything is open. Every work that we do will be publicly discussed and open on GitLab. So thank you all for all the contribution. It was really intense. And I hope that we will make a great MOOC because of this great work from the beginning. So thank you all.

S5: 01:35:47.929 Thank you.

S3: 01:35:48.985 Thanks to you for having that. And, yeah, [inaudible].

S4: 01:35:55.068 Yes.

S6: 01:35:55.640 Thanks, everyone.

S2: 01:35:55.640 Yeah, and [inaudible] a few beers or stuff when we can. We have in the meeting notes the address of Remi for the next open bar at [Paris?].

S3: 01:36:16.150 Sounds great.

S2: 01:36:16.377 If you--

S1: 01:36:17.538 I will have to stop the recording now.

S4: 01:36:20.582 All right. Yes. Yes.

S3: 01:36:21.098 All right.

S2: 01:36:23.960 Bye-bye.

S4: 01:36:25.789 Bye.

S1: 01:36:26.625 Bye.

S6: 01:36:27.355 Bye, everyone.
