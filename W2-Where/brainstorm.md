- for this module you can just go directly to the evaluation is you feel confortable
- what are prerequisites ?
  - command line interface for git ? Rémi Sharrock has a mooc for that.
- this MOOC is focused on CODE contribution not design, not documentation etc...
- it's possible to use git without the command line:
  - web interface with gitab+gitpod IDE (eclipse che) or github+codespace- careful with the cost
  - going beyond a single line edit will need at some point the command line interface -bash, powershell)
- containers are a great way to bootstrap contributions:
  - everything correctly configured for dev
  - for example, database + proper test data etc.
  - docker compose
- important concepts:
  - tree of versions
  - merge and merge request / pull and pull requests
  - distributed: multiples copies, clones, forks (navigation the archeology of forks)
  - clone/fork merge/pull : taking care of synonyms, vocabulary
- hosting ourselves a personalized gitlab for a specific introductory activity
  - LTI + OmniAuth bridge / SSO : possibility to link the LTI anonymous ID sent by edx ?
- look at Oh My Git! + other links in the chat
- quality verification automatic tests: reading the contribution guidelines
- discourage the no community project and no maintener projects: not in scope for the MOOC.
- navigation with the "network of forks" to search the "real" project. Be sure not to be on an unmaintained fork.
- workflow: explaining pushing and failing (well that's not the workflow)
- starting to search for a project to contribute to:
  - Filter them by programming languages you know or want to use
  - Filter them by activity. Try to see which one you see are active.
  - possible questions or FLOWCHART (steps and questions to ask yourself):
    - "What is the date for the last commit?
    - "Is there an answer in the pull requests that are open?"
    - "Is there an IRC channel?"
 - Where is the Git? What is the branch? Is it working? good fork or not ?
 - GOURCE: the video visualization of the history of commits
 - vim + gitgrab








